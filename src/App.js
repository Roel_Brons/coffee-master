import React, { Component } from 'react';
import {Router, Route, IndexRoute, hashHistory} from 'react-router';
import Container from './Container';
import Roasters from './Roasters';
import Orders from './Orders';
import Featured from './Featured';
import RoasterEdit from './Roaster/Edit';
import RoasterCreate from './Roaster/Create';
import CoffeeEdit from './Coffee/Edit';
import CoffeeCreate from './Coffee/Create';
import FeaturedEdit from './Featured/Edit';
import FeaturedCreate from './Featured/Create';

export default class App extends Component {
  
	render() {
		return (
			<Router history={hashHistory}>
				<Route path="/" component={Container}>
					<IndexRoute component={Roasters} />
					<Route path="/roaster/edit/:id" component={RoasterEdit} />
					<Route path="/roaster/create" component={RoasterCreate} />
					<Route path="/coffee/edit/:id" component={CoffeeEdit} />
					<Route path="/coffee/create" component={CoffeeCreate} />
					<Route path="/orders" component={Orders} />
					<Route path="/featured" component={Featured} />
					<Route path="/featured/edit/:id" component={FeaturedEdit} />
					<Route path="/featured/create" component={FeaturedCreate} />
				</Route>
			</Router>
		);
	}

}