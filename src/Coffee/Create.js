import React, {Component} from 'react';
import Form from './Form';
import request from 'request';
import { hashHistory } from 'react-router';
import { apiEndpoint } from '../config';
import 'sweetalert/dist/sweetalert.css';
const swal = require('sweetalert/lib/sweetalert');

export default class CreatePage extends Component {

	constructor(props) {
		super(props);
		this.state = {formData: {}};
	}

	onFormChange(formData) {
		this.setState({formData});
		
	}

	onSave() {
		const { roaster } = this.props.location.query;
		var shipping = {
			company_name: this.state.formData.company_name, 
			address: this.state.formData.address, 
			state: this.state.formData.state, 
			zip: this.state.formData.zip, 
			country: this.state.formData.country, 
			city: this.state.formData.city, 
			phone: this.state.formData.phone,
			email: this.state.formData.email, 
			width: this.state.formData.width, 
			height: this.state.formData.height, 
			weight: this.state.formData.weight, 
			length: this.state.formData.length
		}
		let { name, description, photoUrl, price, roast} = this.state.formData;

		// calculate stripe fee 

		// price without strip fee in cents
		var price_bare = price * 100;

		// calculate stripe fee 
		var stripe_fee = Math.ceil((price_bare * 2.9)/100) + 30; 

		// calculate item price with stripe fee included    
		price = parseFloat(((price_bare + stripe_fee) / 100).toFixed(2)); 


		request({
			method: 'POST',
			uri: `${apiEndpoint}/coffee`,
			headers: {
		        'Content-Type': 'application/json'
	    	},
			body: JSON.stringify({ name, description, photoUrl, price, roaster, roast, shipping}),
		}, (err, res, body) => {
			if(err || res.statusCode === 400){
				swal("Oh no...", `There's been an error creating this coffee.`, 'error');
				return console.log('[CreatePage]', err);
			}
			swal({
				title: "Nice!",
				text: `${name} has been created.`,
				type: 'success'
			}, () => {
				hashHistory.push('/');
			});
		})
	}

	render() {
		const {roasterName} = this.props.location.query;
		return (
			<div className="create-page">
				<div className="page-header create-page-header">
						<h3>New Coffee: <span className="coffee-from">{roasterName}</span></h3>
						
					<button className="article-create-btn button-primary" onClick={this.onSave.bind(this)}>Create</button>
				</div>
				<Form onChange={this.onFormChange.bind(this)} data={this.state.formData} />
			</div>
		)
	}
}