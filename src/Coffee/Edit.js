import React, {Component} from 'react';
import Form from './Form';
import request from 'request';
import { apiEndpoint } from '../config';
import { hashHistory } from 'react-router';
import 'sweetalert/dist/sweetalert.css';
const swal = require('sweetalert/lib/sweetalert');
// import '../css/EditPage.css';

export default class EditPage extends Component {

	constructor() {
		super();
		this.state = {coffee: {}, formData: {}};
	}

	componentWillMount() {
		const id = this.props.params.id;
		request(`${apiEndpoint}/coffee/${id}`, (err, res, body) => {
			
				var coffee = JSON.parse(body); 
				var data = Object.assign({}, coffee.shipping); 
				console.log(data)
				
				console.log(coffee_data)
				
				if (typeof coffee.original_price != 'undefined') {
					coffee.price = coffee.original_price; 
				}

				var coffee_data = Object.assign({}, coffee, data); 

				this.setState({coffee: coffee, formData: coffee_data})
			
			} )
	}

	onFormChange(formData) {
		this.setState({formData});
	}

	onSave() {
		var shipping = {
			company_name: this.state.formData.company_name, 
			address: this.state.formData.address, 
			state: this.state.formData.state, 
			zip: this.state.formData.zip, 
			country: this.state.formData.country, 
			city: this.state.formData.city, 
			phone: this.state.formData.phone,
			email: this.state.formData.email,
			width: this.state.formData.width, 
			height: this.state.formData.height, 
			weight: this.state.formData.weight, 
			length: this.state.formData.length			
		}
		

		
		let { name, description, photoUrl, price, roaster, roast } = this.state.formData;
		const id = this.props.params.id

		// calculate stripe fee 

		// price without strip fee in cents
		var price_bare = price * 100;

		// calculate stripe fee 
		var stripe_fee = Math.ceil((price_bare * 2.9)/100) + 30; 

		// calculate item price with stripe fee included    
		price = parseFloat(((price_bare + stripe_fee) / 100).toFixed(2)); 
		
		request({
			method: 'PUT',
			uri: `${apiEndpoint}/coffee/${id}`,
			headers: {
		        'Content-Type': 'application/json'
	    	},
			body: JSON.stringify({ name, description, photoUrl, price, roast, shipping }),
		}, (err, res, body) => {
			if(err || res.statusCode === 400) {
				swal("Oh no...", `There's been an error saving ${name}`, 'error');
				return console.log('[EditPage]', err);
			}
			swal("Nice!", `Changes for ${name} has been saved.`, 'success');
		})
	}

	onDelete() {
		const id = this.props.params.id;
		const name = this.state.formData.name;
		swal({
			title: 'Are you sure?',
			text: `Once you delete ${name}, there's no going back...`, 
			type: 'warning',
			closeOnConfirm: false,
			disableButtonsOnConfirm: true,
			showCancelButton: true,
		}, () => {
			request({
				method: 'DELETE',
				uri: `${apiEndpoint}/coffee/${id}`,
			}, (err, res, body) => {
				if(err || res.statusCode === 400){
					swal("Oh no...", `There's been an problem deleting ${name}`, 'error');
					return console.log('[EditPage]', err);
				}
				swal({
					title: "Nice!",
					text: `${name} has been deleted.`,
					type: 'success'
				}, () => {
					hashHistory.push('/');
				});
			})
		});
	}

	render() {
		const {roasterName} = this.state.formData;
		return (
			<div className="edit-page">
				<div className="page-header edit-page-header">
					<h3>Edit Coffee</h3>
					<div>
						<button className="save-button button-primary" onClick={this.onSave.bind(this)}>Save</button>
						<button className="delete-button" onClick={this.onDelete.bind(this)}>Delete</button>
					</div>
				</div>
				<Form onChange={this.onFormChange.bind(this)} data={this.state.formData} />
			</div>
		)
	}
}