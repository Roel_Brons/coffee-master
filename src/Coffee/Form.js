import React, { Component } from 'react';
import AutoForm from '../Form/AutoForm'
import TextInput from '../Form/TextInput';
import TextArea from '../Form/TextArea';
import NumberInput from '../Form/NumberInput';
import RadioInput from '../Form/RadioInput';
// import '../css/ArticleForm.css';
const roastOptions = [
	{label: 'Light', value: 'light'},
	{label: 'Medium', value: 'medium'},
	{label: 'Dark', value: 'dark'},
	{label: 'Other', value: 'other'},
]

export default class Form extends Component {

	render() {
		const { name, description, photoUrl, price, roast, company_name, address, state, zip, country, city, phone, email, width, height, weight, length} = this.props.data;
		return (
			<div className="flex-center">
				<AutoForm onChange={this.props.onChange} value={this.props.data}>
					<p className="form-subtitle">General information:</p>
					<TextInput id="coffee-form-name" value={name} label="Name" property="name" />
					<TextArea id="coffee-form-description" value={description} label="Description" property="description" />
					<TextInput id="coffee-form-photo" value={photoUrl} label="Photo URL" property="photoUrl" />
					<NumberInput id="coffee-form-price" value={price} label="Price" property="price" />
					<RadioInput id="coffee-form-roast" value={roast} label="Roast" property="roast" options={roastOptions} /> 
					<p className="form-subtitle">Shipping information: </p>
					<TextInput id="coffee-form-cname" value={company_name} label="Company name" property="company_name" />
					<TextInput id="coffee-form-country" value={country} label="Country" property="country" />
					<TextInput id="coffee-form-city" value={city} label="City" property="city" />
					<TextArea id="coffee-form-address" value={address} label="Address" property="address" />
					<TextInput id="coffee-form-state" value={state} label="State" property="state" /> 
					<TextInput id="coffee-form-zip" value={zip} label="Zip" property="zip" /> 
					<TextInput id="coffee-form-phone" value={phone} label="Phone" property="phone" /> 
					<TextInput id="coffee-form-email" value={email} label="Email" property="email" />
					<p className="form-subtitle">Product dimensions: </p>
					<TextInput id="coffee-form-weight" value={weight} label="Product weight(oz)" property="weight" />
					<TextInput id="coffee-form-height" value={height} label="Product height(in)" property="height" />
					<TextInput id="coffee-form-width" value={width} label="Product width(in)" property="width" />
					<TextInput id="coffee-form-width" value={length} label="Product length(in)" property="length" />
				</AutoForm>
			</div>

		);
	}
}