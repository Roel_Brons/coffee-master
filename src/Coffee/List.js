import React, { Component } from 'react';
import ListItem from './ListItem';
import {Link} from 'react-router';
// import '../css/ArticleList.css'

export default class List extends Component {

	getListItems(roasters) {
		if(!roasters || roasters.length <= 0) return <p>There are currently no roasters.</p>
		return roasters.map(d => <ListItem key={d._id} data={d} />)
	}

	render() {
		const items = this.getListItems(this.props.roasters);
		return (
			<div className="article-list">
				<div className="article-list-header">
					<h2>Roasters</h2>
					<Link to="/roaster/create" className="button button-primary new-article-btn">New Roaster</Link>
				</div>
				{items}
			</div>
		);
	}
}
