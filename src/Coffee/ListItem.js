import React, { Component } from 'react';
import {Link} from 'react-router';
import moment from 'moment';
// import '../css/ArticleListItem.css'

export default class ListItem extends Component {
	render() {
		const { _id, name, url, shortDescription, photoUrl, createdAt } = this.props.data;
		return (
			<div className="article-list-item">
				<div className="article-list-item-header">
					<div className="article-title">{name}</div>
					<Link className="article-edit-button"to={`roaster/edit/${_id}`}>Edit</Link>
				</div>
				<div className="article-list-item-body">
					<div className="article-description">{shortDescription}</div>
					<div className="article-date">{moment(createdAt).format('MMMM Do, YYYY')}</div>
				</div>
			</div>
		);
	}
}
