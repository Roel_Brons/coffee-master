import React, { Component } from 'react';
import TableRow from './TableRow';
// import '../css/Table.css'

export default class Table extends Component {

	render() {
		const {data} = this.props;
		console.log('[Table] Rendering coffee table');
		if(!data || data.length <= 0) return <p style={{textAlign: 'center'}}>Sorry, no coffees yet!</p>
		return (
			<table className="table">
				<thead>
					<tr>
						<th>Name</th>
						<th>Price</th>
						<th>Roast</th>
						<th className="align-right">Edit</th>
					</tr>
				</thead>
				<tbody>
					{ data.map(d => <TableRow key={d._id} data={d}/>) }
				</tbody>
			</table>
		);
	}
}
