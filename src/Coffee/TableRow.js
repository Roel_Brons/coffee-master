import React, { Component } from 'react';
import {Link} from 'react-router';
// import '../css/TableRow.css'

export default class TableRow extends Component {

	render() {
		const {_id, name, price, roast} = this.props.data;
		return (
			<tr>
				<td>{name}</td>
				<td>{'$'+price}</td>
				<td style={{textTransform: 'capitalize'}}>{roast}</td>
				<td className="align-right">
					<Link to={`/coffee/edit/${_id}`}>Edit</Link>
				</td>
			</tr>
		);
	}
}
