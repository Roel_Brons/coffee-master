import React, {Component} from 'react';
import {Link} from 'react-router';

export default class Container extends Component {
	render() {
		return (
			<div className="container">
				<nav className="page-nav">
					<Link to="/">Roasters</Link>
					<Link to="/orders">Orders</Link>
					<Link to="/featured">Featured</Link>
				</nav>
				{this.props.children}
			</div>
		)
	}
}