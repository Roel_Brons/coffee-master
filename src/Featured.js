import React, {Component} from 'react';
import FeaturedList from './Featured/List';
import request from 'request';
import { apiEndpoint } from './config';

export default class Featured extends Component {

	constructor() {
		super();
		this.state = {featured: null};
	}

	componentWillMount() {
		request(`${apiEndpoint}/featured`, (err, res, body) => this.setState({featured: JSON.parse(body)}) )
	}

	render() {
		return (
			<div className="admin-page">
				<FeaturedList featured={this.state.featured} />
			</div>
		)
	}
}