import React, {Component} from 'react';
import Form from './Form';
import request from 'request';
import { apiEndpoint } from '../config';
import { hashHistory } from 'react-router';
import 'sweetalert/dist/sweetalert.css';
const swal = require('sweetalert/lib/sweetalert');

export default class CreatePage extends Component {

	constructor(props) {
		super(props);
		this.state = {formData: {}};
	}

	onFormChange(formData) {
		this.setState({formData});
	}

	onSave() {
		const { url, photoUrl, name } = this.state.formData;

		request({
			method: 'POST',
			uri: `${apiEndpoint}/featured`,
			headers: {
		        'Content-Type': 'application/json'
	    	},
			body: JSON.stringify({ url, photoUrl, name }),
		}, (err, res, body) => {
			if(err || res.statusCode === 400){
				swal("Oh no...", `There's been an error creating this roaster.`, 'error');
				return console.log('[CreatePage]', err)
			}
			swal({
				title: "Nice!",
				text: `${name} has been created.`,
				type: 'success'
			}, () => {
				hashHistory.push('/');
			});
		})
	}

	render() {
		return (
			<div className="create-page">
				<div className="page-header create-page-header">
					<h3>New Featured Content</h3>
					<button className="create-button button-primary" onClick={this.onSave.bind(this)}>Create</button>
				</div>
				<div className="featured-image-container">
					<img src={this.state.formData.photoUrl} alt="Featured Content"/>
				</div>
				<Form onChange={this.onFormChange.bind(this)} data={this.state.formData} />
			</div>
		)
	}
}