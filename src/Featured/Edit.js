import React, {Component} from 'react';
import Form from './Form';
import request from 'request';
import { apiEndpoint } from '../config';
import { hashHistory } from 'react-router';
import 'sweetalert/dist/sweetalert.css';
const swal = require('sweetalert/lib/sweetalert');
// import '../css/EditPage.css';

export default class EditPage extends Component {

	constructor() {
		super();
		this.state = {featured: {}, formData: {}};
	}

	componentWillMount() {
		const id = this.props.params.id
		request(`${apiEndpoint}/featured/${id}`, (err, res, body) => this.setState({featured: JSON.parse(body), formData: JSON.parse(body)}) )
	}

	onFormChange(formData) {
		this.setState({formData});
	}

	onSave() {
		console.log('[EditPage] Saving featured content...');
		const { url, photoUrl, name } = this.state.formData;
		const id = this.props.params.id

		request({
			method: 'PUT',
			uri: `${apiEndpoint}/featured/${id}`,
			headers: {
		        'Content-Type': 'application/json'
	    	},
			body: JSON.stringify({ url, photoUrl, name }),
		}, (err, res, body) => {
			if(err || res.statusCode === 400) {
				swal("Oh no...", `There's been an error saving ${name}`, 'error');
				return console.log('[EditPage]', err);
			}
			swal("Nice!", `Changes for ${name} has been saved.`, 'success');
		})
	}

	onDelete() {
		const id = this.props.params.id;
		const name = this.state.formData.name;
		swal({
			title: 'Are you sure?',
			type: 'warning',
			closeOnConfirm: false,
			disableButtonsOnConfirm: true,
			showCancelButton: true,
		}, () => {
			request({
				method: 'DELETE',
				uri: `${apiEndpoint}/featured/${id}`,
			}, (err, res, body) => {
				if(err || res.statusCode === 400) {
					swal("Oh no...", `There's been an problem deleting ${name}`, 'error');
					return console.log('[EditPage]', err);
				}
				swal({
					title: "Nice!",
					text: `${name} has been deleted.`,
					type: 'success'
				}, () => {
					hashHistory.push('/');
				});
			})
		})
		
	}

	render() {
		console.log('[EditPage] Rendering...');
		return (
			<div className="edit-page">
				<div className="page-header edit-page-header">
					<h3>Edit Featured Content</h3>
					<div>
						<button className="save-button button-primary" onClick={this.onSave.bind(this)}>Save</button>
						<button className="delete-button" onClick={this.onDelete.bind(this)}>Delete</button>
					</div>
				</div>
				<div className="featured-image-container">
					<img src={this.state.formData.photoUrl} alt="Featured Content"/>
				</div>
				<Form onChange={this.onFormChange.bind(this)} data={this.state.formData} />
			</div>
		)
	}
}