import React, { Component } from 'react';
import AutoForm from '../Form/AutoForm'
import TextInput from '../Form/TextInput';

export default class Form extends Component {

	render() {
		const { url, photoUrl, name, createdAt } = this.props.data;
		return (
			<div className="flex-center">
				<AutoForm onChange={this.props.onChange} value={this.props.data}>
					<TextInput id="featured-form-name" value={name} label="Name" property="name" />
					<TextInput id="featured-form-photo" value={photoUrl} label="Photo URL" property="photoUrl" />
					<TextInput id="featured-form-url" value={url} label="URL" property="url" />
				</AutoForm>
			</div>

		);
	}
}

					// <NumberInput id="roaster-form-lat" value={lat} label="Latitude" property="" />
					// <NumberInput id="roaster-form-lng" value={lng} label="Longitude" property="name" />