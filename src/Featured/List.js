import React, { Component } from 'react';
import ListItem from './ListItem';
import {Link} from 'react-router';

export default class List extends Component {

	getListItems(featured) {
		if(!featured || featured.length <= 0) return <p>There is currently no featured content.</p>
		const sortedContent = featured.slice().sort((a, b) => Date.parse(b.createdAt) - Date.parse(a.createdAt))
		return sortedContent.map(d => <ListItem key={d._id} data={d} />);
	}

	render() {
		const items = this.getListItems(this.props.featured);
		return (
			<div className="list featured-list">
				<div className="page-header">
					<h3>Featured</h3>
					<Link to="/featured/create" className="button button-primary">New Featured Content</Link>
				</div>
				<div className="flex-center-column-hz">
					{items}
				</div>
			</div>
		);
	}
}
