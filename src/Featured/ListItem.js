import React, { Component } from 'react';
import { Link } from 'react-router';

export default class ListItem extends Component {

	render() {
		const { _id, url, photoUrl, name, createdAt} = this.props.data;
		return (
			<div className='featured-item'>
				<img src={photoUrl} alt="Featured Content" className='featured-item-img'/>
				<div className="featured-item-info">
					<a href={url} target='_' className="featured-item-name">{name}</a>
					<Link to={`featured/edit/${_id}`} className="featured-item-edit">Edit</Link>
				</div>
			</div>
		);
	}
}
