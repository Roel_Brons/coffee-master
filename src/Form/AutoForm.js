import React, { Component } from 'react';

// AutoForm is a higher level component that accepts form inputs as children,
// binding event listeners to them that updates the form object.

export default class AutoForm extends Component {

	changeFactory(property) {
		const onChangeFunc = (e) => {
			let {value, onChange} = this.props;
			let propertyData = {};
			propertyData[property] = e.target.value;
			onChange(Object.assign({}, value, propertyData))
		}
		return onChangeFunc;
	}

	render() {
		return (
			<form className="auto-form">
				{React.Children.map(this.props.children, (child) => {
					return React.cloneElement(child, {
						value: child.props.value || this.props.value[child.props.property] || null,
						onChange: this.changeFactory(child.props.property).bind(this)})
				})}
			</form>

		);
	}
}
