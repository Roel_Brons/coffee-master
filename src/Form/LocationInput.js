import React, { Component } from 'react';
import NumberInput from './NumberInput';
import { getCityState, getStaticMap } from '../mapboxUtil'
// import '../css/TextInput.css';

// As a composite form component, we'll need to fake being one component for AutoForm.
// This entails calling onChange manually with each change, as well as using a fake event argument for e.target.value

export default class LocationInput extends Component {

	changeFactory(property) {
		const onChangeFunc = (e) => {
			let {value, onChange} = this.props;
			let propertyData = {};

			propertyData[property] = e.target.value;
			const o = Object.assign({}, value, propertyData);

			onChange({target: {value: o}});
			this.updateCityState(o, o);
		}
		return onChangeFunc;
	}

	updateCityState(newValue, {lat, lng}) {
		const { onChange } = this.props;
		lat = Number(lat) || 0;
		lng = Number(lng) || 0;
		getCityState(lat, lng, (err, res) => {
			if(err) return console.error(err);
			const o = Object.assign({}, newValue, res);
			onChange({target: {value: o}});
		})
	}

	render() {
		const { lat, lng, city, state } = this.props.value;
		return (
			<div className="form-control location-input flex-center">
				<div className="flex-center-column">
					<div className="location-input-controls">
						<NumberInput id={this.props.id + '-lat'} value={lat || null} label="Latitude" onChange={this.changeFactory('lat').bind(this)}/>
						<NumberInput id={this.props.id + '-lng'} value={lng || null} label="Longitude" onChange={this.changeFactory('lng').bind(this)}/>
					</div>
					<p className="location-input-city-state"><b>City:</b> {city}&nbsp;&nbsp;<b>State:</b> {state}</p>
				</div>
				<img src={getStaticMap(lat, lng)} alt="A map of the chosen location"/>
			</div>
		);
	}
}
