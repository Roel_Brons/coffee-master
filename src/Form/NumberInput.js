import React, { Component } from 'react';
// import '../css/TextInput.css';

export default class NumberInput extends Component {

	render() {
		const { value, label, id, placeholder, onChange } = this.props;
		return (
			<div className="form-control number-form-control">
				<label className="text-input-label" htmlFor={id}>{label}</label>
				<input 
					type="number"
					id={id}
					className="number-input"
					placeholder={ placeholder || ''}
					value={value || ''}
					onChange={ onChange } />
			</div>

		);
	}
}
