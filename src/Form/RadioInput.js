import React, { Component } from 'react';
// import '../css/TextInput.css';

export default class RadioInput extends Component {

	render() {
		const { value, label, id, onChange, options } = this.props;
		return (
			<div className="form-control radio-form-control">
				<label className="radio-field-label">{label}</label>
				{options.map(i => (
					<div key={id+'-'+i.value} className="radio-group">
						<input 
							type="radio"
							name={label}
							id={id+'-'+i.value}
							className="radio-input"
							value={i.value}
							checked={value === i.value}
							onChange={ onChange } />
						<label className="radio-input-label" htmlFor={id+'-'+i.value}>{i.label}</label>
					</div>
				))}
			</div>

		);
	}
}
