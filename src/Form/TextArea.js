import React, { Component } from 'react';
// import '../css/TextArea.css';

export default class TextArea extends Component {

	render() {
		const { value, label, id, placeholder, onChange } = this.props;
		return (
			<div className="form-control textarea-form-control">
				<label className="text-area-label" htmlFor={id}>{label}</label>
				<textarea 
					type="text"
					id={id}
					className="text-area"
					placeholder={ placeholder || ''}
					value={value || ''}
					onChange={ onChange } >
				</textarea>
			</div>

		);
	}
}
