import React, { Component } from 'react';
// import '../css/TextInput.css';

export default class TextInput extends Component {

	render() {
		const { value, label, id, placeholder, onChange } = this.props;
		return (
			<div className="form-control text-form-control">
				<label className="text-input-label" htmlFor={id}>{label}</label>
				<input 
					type="text"
					id={id}
					className="text-input"
					placeholder={ placeholder || ''}
					value={value || ''}
					onChange={ onChange } />
			</div>

		);
	}
}
