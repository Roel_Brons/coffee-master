import React, { Component } from 'react';
import ListItem from './ListItem';
import {Link} from 'react-router';

export default class List extends Component {

	getListItems(orders) {
		
		console.log(orders); 
		if(!orders || orders.length <= 0) return <p>There are currently no orders.</p>
		const sortedOrders = orders.slice().sort((a, b) => Date.parse(b.createdAt) - Date.parse(a.createdAt))
		return sortedOrders.map(d => <ListItem key={d._id} data={d} updateStatus={this.props.updateStatus} />);
	}

	render() {
		const items = this.getListItems(this.props.orders);
		return (
			<div className="list order-list">
				<div className="page-header">
					<h3>Orders</h3>
				</div>
				{items}
			</div>
		);
	}
}
