import React, { Component } from 'react';
import Reveal from '../Reveal';
import OrderTable from './OrderTable';
import { Link } from 'react-router';
import { stripeURL } from '../config';
import moment from 'moment';

export default class ListItem extends Component {
	
	constructor(props) {
		super(props);
		this.state = {active: false}
	}

	toggleReveal() {
		this.setState({active: !this.state.active});
	}

	getOrderStatusText(status) {
		if (status === 0) return "Not Processed";
		else if (status === 1) return "In Progress";
		else if (status === 2) return "Complete"
		else return "Unknown";
	}

	updateStatus() {
		const { updateStatus, data } = this.props;
		let nextStatus;
		if(data.orderStatus === 0) nextStatus = 1;
		else if(data.orderStatus === 1) nextStatus = 2;
		else if(data.orderStatus === 2) nextStatus = 0;
		else nextStatus = 0;
		this.props.updateStatus(data._id, nextStatus);
	}

	render() {
		const { _id, email, shipping, orderStatus, paymentStatus, items, createdAt, chargeId } = this.props.data;
		return (
			<div className={`order-item order-status-${orderStatus}`}>
				<div className="order-item-info">
					<div className="order-item-name"><a href={`mailto:${email}`}>{shipping.name}</a></div>
					<div className="order-status flex-spacer" onClick={this.updateStatus.bind(this)}>({this.getOrderStatusText(orderStatus)})</div>
					<a href={stripeURL + chargeId} target="_blank" className={'order-payment-status '+(paymentStatus ? 'pay-success' : 'pay-failed')}>{paymentStatus ? 'Successful Payment' : 'Payment Failed'}</a>
				</div>
				<div className="order-item-shipping">
					<span>Address: </span>
					{`${shipping.line1}, ${shipping.city} ${shipping.state}, ${shipping.zip}`}
				</div>
				<div className="order-item-footer">
					<div className="order-item-date flex-spacer">{`Ordered on ${moment(createdAt).format('MMMM Do, YYYY')}`}</div>
					<div className="order-item-toggle" onClick={this.toggleReveal.bind(this)}>{`${items.reduce((t, i) => t + i.quantity, 0)} Items Ordered`}</div>
				</div>
				<Reveal active={this.state.active}>
					<OrderTable data={items} />
				</Reveal>
			</div>
		);
	}
}
