import React, { Component } from 'react';
export default class OrderTable extends Component {

	getTotalPrice(items) {
		
		var sum = items.reduce(add, 0);

		function add(a, b) {

			if (b.bean) {
				return a + b.bean.price; 
			}
			else return 0
		}

		return sum.toFixed(2); 
		
	}

	render() {
		const { data } = this.props;
		return (
			<table className="order-item-cart table">
				<thead>
					<tr>
						<th>Product</th>
						<th>Quantity</th>
						<th>Price</th>
					</tr>
				</thead>
				<tbody>
					{data.map(i => <tr key={Math.random() * 10000}>
							<td>{i.bean ? i.bean.name : 'Coffee'}<span className='order-cart-roaster'>{i.bean ? i.bean.roaster.name : 'Roaster'}</span></td>
							<td>{i.quantity}</td>
							<td>{'$'+ (i.bean ? i.bean.price : '0')}</td>
						</tr>)}
				</tbody>
				<tfoot>
					<tr>
						<td>Total</td>
						<td></td>
						<td>{'$'+this.getTotalPrice(data)}</td>
					</tr>
				</tfoot>
			</table>
		);
	}
}
