import React, {Component} from 'react';
import OrderList from './Order/List';
import request from 'request';
import { apiEndpoint } from './config';

export default class Orders extends Component {

	constructor() {
		super();
		this.state = {orders: null};
	}

	componentWillMount() {
		request(`${apiEndpoint}/order`, (err, res, body) => this.setState({orders: JSON.parse(body)}) )
	}

	updateStatus(id, orderStatus) {
		const { orders } = this.state;
		request({
			method: 'PUT',
			uri: `${apiEndpoint}/order/${id}`,
			headers: {
		        'Content-Type': 'application/json'
	    	},
			body: JSON.stringify({ orderStatus }),
		}, (err, res, body) => {
			if(err) return console.error(err);
			const newOrders = orders.map(i => {
				if(i._id === id) return Object.assign({}, i, {orderStatus});
				else return i;
			})
			console.log(newOrders);
			this.setState({orders: newOrders})
		});
	}

	render() {
		return (
			<div className="admin-page">
				<OrderList orders={this.state.orders} updateStatus={this.updateStatus.bind(this)} />
			</div>
		)
	}
}