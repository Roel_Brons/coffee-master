import React, { Component } from 'react';

export default class Reveal extends Component {
  
	render() {
		const { active, children } = this.props;
		return (
			<div className={'reveal ' + (active ? 'reveal-active' : '')}>
				{children}
			</div>
		);
	}

}