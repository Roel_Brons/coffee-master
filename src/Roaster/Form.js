import React, { Component } from 'react';
import AutoForm from '../Form/AutoForm'
import TextInput from '../Form/TextInput';
import TextArea from '../Form/TextArea';
import LocationInput from '../Form/LocationInput';
// import '../css/ArticleForm.css';

export default class Form extends Component {

	render() {
		const { name, shortDescription, url, photoUrl, story, location } = this.props.data;
		return (
			<div className="flex-center">
				<AutoForm onChange={this.props.onChange} value={this.props.data}>
					<TextInput id="roaster-form-name" value={name} label="Name" property="name" />
					<TextArea id="roaster-form-description" value={shortDescription} label="Description" property="shortDescription" />
					<TextInput id="roaster-form-url" value={url} label="URL" property="url" />
					<TextInput id="roaster-form-photo" value={photoUrl} label="Photo URL" property="photoUrl" />
					<LocationInput id="roaster-form-location" value={location || {}} property="location" />
					<TextArea id="roaster-form-story" value={story} label="Story" property="story" />
				</AutoForm>
			</div>

		);
	}
}

					// <NumberInput id="roaster-form-lat" value={lat} label="Latitude" property="" />
					// <NumberInput id="roaster-form-lng" value={lng} label="Longitude" property="name" />