import React, { Component } from 'react';
import ListItem from './ListItem';
import {Link} from 'react-router';

export default class List extends Component {

	getListItems(roasters) {
		if(!roasters || roasters.length <= 0) return <p>There are currently no roasters.</p>
		const sortedRoasters = roasters.slice().sort((a, b) => Date.parse(b.createdAt) - Date.parse(a.createdAt))
		return sortedRoasters.map(d => <ListItem key={d._id} data={d} />);
	}

	render() {
		const items = this.getListItems(this.props.roasters);
		return (
			<div className="list roaster-list">
				<div className="page-header">
					<h3>Roasters</h3>
					<Link to="/roaster/create" className="button button-primary">New Roaster</Link>
				</div>
				{items}
			</div>
		);
	}
}
