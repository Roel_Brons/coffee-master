import React, { Component } from 'react';
import Reveal from '../Reveal';
import CoffeeTable from '../Coffee/Table';
import {Link} from 'react-router';
import moment from 'moment';
// import '../css/ListItem.css';

export default class ListItem extends Component {
	constructor(props) {
		super(props);
		this.state = {active: false}
	}
	toggleReveal() {
		this.setState({active: !this.state.active});
	}
	render() {
		const { _id, name, url, shortDescription, photoUrl, createdAt, location, coffees} = this.props.data;
		const nameComponent = url
			? <a target="_blank" href={url} className="list-item-title">{name}</a>
			: <div className="list-item-title">{name}</div>
		return (
			<div className="list-item">
				<div className="list-item-main">
					<img src={photoUrl} alt={`Represents ${name}`} className="list-item-img"/>
					<div className="list-item-content">
						<div className="list-item-header">
							{nameComponent}
							<Link className="edit-roaster-button" to={`roaster/edit/${_id}`}>Edit</Link>
						</div>
						<div className="list-item-body">
							<div className="list-item-description">{shortDescription}</div>
							<div className="list-item-info">
								<span className="list-item-location">{`${location.city}, ${location.state}`}</span>
								<span className="list-item-date">{`(${moment(createdAt).format('MMMM Do, YYYY')})`}</span>
								<span className="flex-spacer"></span>
								<span className="list-item-coffee-count" onClick={this.toggleReveal.bind(this)}>{coffees.length + ' coffees'}</span>
								<Link className="new-coffee-button" to={`coffee/create?roaster=${_id}&roasterName=${name.split(' ').join('%20')}`}>New Coffee</Link>
							</div>
						</div>
					</div>
				</div>
				<Reveal active={this.state.active}>
					<CoffeeTable data={coffees} />
				</Reveal>
			</div>
		);
	}
}
