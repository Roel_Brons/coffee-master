import React, {Component} from 'react';
import RoasterList from './Roaster/List';
import request from 'request';
import { apiEndpoint } from './config';

export default class Roasters extends Component {

	constructor() {
		super();
		this.state = {roasters: null};
	}

	componentWillMount() {
		request(`${apiEndpoint}/roaster`, (err, res, body) => this.setState({roasters: JSON.parse(body)}) )
	}

	render() {
		return (
			<div className="admin-page">
				<RoasterList roasters={this.state.roasters} />
			</div>
		)
	}
}