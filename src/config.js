export const apiEndpoint = 'http://localhost:5000';
export const mapboxKey = 'pk.eyJ1Ijoibmlja3JvYmVydHM0MDQiLCJhIjoiMC1kS1RXdyJ9.lbCLAKKF-uTLu6Hx-0Ppbg';
export const stripeURL = 'https://dashboard.stripe.com/test/payments/';
