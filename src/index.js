import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './css/normalize.css';
import './css/skeleton.css';
import './css/global.css';

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
