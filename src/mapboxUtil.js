import { mapboxKey } from './config';
const MapboxGeocoding = require('mapbox/lib/services/geocoding');
const client = new MapboxGeocoding(mapboxKey);

export const getCityState = (lat, lng, cb) => {
	client.geocodeReverse({latitude: lat, longitude: lng}, {types: 'place,region'}, (err, res) => {
		if(err) return cb(err);
		if(res.features.length <= 0) return false;
		// Takes the response's feature set and transforms it into an object like {city: 'Austin', state: 'Texas'}
		const data = res.features.map(f => f.id.startsWith('place') ? {city: f.text} : {state: f.text}).reduce((o1, o2) => Object.assign(o1, o2), {});
		cb(undefined, data);
	});
}

export const getStaticMap = (lat, lng) => {
	return `https://api.mapbox.com/v4/mapbox.streets/pin-m-cafe+FF5656(${lng},${lat})/${lng},${lat},15/200x200.png?access_token=${mapboxKey}`
}